package Model;

public class Model {
 private int ID;
 private String USER;
 private String PASS;
 private String NAME;
 private String PHONE;
 private String ADDRESS;

    public Model() {
    }

 
    public Model(String USER, String PASS) {
        this.USER = USER;
        this.PASS = PASS;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

public String getUSER() {
	return USER;
}
public void setUSER(String uSER) {
	USER = uSER;
}
public String getPASS() {
	return PASS;
}
public void setPASS(String pASS) {
	PASS = pASS;
}
public String getNAME() {
	return NAME;
}
public void setNAME(String nAME) {
	NAME = nAME;
}

public String getPHONE() {
	return PHONE;
}
public void setPHONE(String pHONE) {
	PHONE = pHONE;
}
public String getADDRESS() {
	return ADDRESS;
}
public void setADDRESS(String aDDRESS) {
	ADDRESS = aDDRESS;
}
 
}

package UserService;

import java.util.List;

import Controller.UserDao;
import Model.Model;

public class UserSV {
 private UserDao userdao;
 
 public UserSV() {
	 userdao=new UserDao();
 }
 public List<Model>getAllUsers(){
	 return userdao.getAllUsers();	 
 }
 public int addUsers(Model user){
	 return userdao.addUsers(user);

 }
 public void delete(int id){
	  userdao.delete(id);	 
 }
public Model getuserID(int id){
    return userdao.getid(id);
}
 public void update(Model user){
	  userdao.update(user);	 
 }
 public Model getIDbyuser(String username){
    return userdao.getidbyusername(username);
}
}

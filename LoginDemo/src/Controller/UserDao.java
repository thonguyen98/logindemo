package Controller;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;
import Model.Model;

public class UserDao {
	public static int signin(Model user) {
		int kq=-1;
		Connection connection=ConnectDB.getConnect();
		String sql="INSERT INTO user VALUES(?,?)";
		try {
			PreparedStatement ps= connection.prepareStatement(sql);
			ps.setString(1, user.getUSER());
			ps.setString(2, user.getPASS());
			kq=ps.executeUpdate();
		}catch(Exception e) {
			
		}
		return kq;
	}
	public static int login(Model user) {
	    	int kq=-1;
		Connection connection=ConnectDB.getConnect();
		String sql="SELECT USERNAME,PASS FROM user WHERE USERNAME=? AND PASS=?";
		try {
			PreparedStatement ps= connection.prepareStatement(sql);
			ps.setString(1, user.getUSER());
			ps.setString(2, user.getPASS());
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				kq=1;
			}
                        else{
                        String sqladmin="SELECT USERNAME,PASS FROM admin WHERE USERNAME=? AND PASS=?";
                        try {
                        	ps= connection.prepareStatement(sqladmin);
							ps.setString(1, user.getUSER());
							ps.setString(2, user.getPASS());
							rs = ps.executeQuery();
                        if(rs.next()) {
				kq=2;
			} 
                        }
                        catch(Exception e) {
                        }
                        }
		}catch(Exception e) {
		}
		return kq;
	}
	public List<Model> getAllUsers(){
		List<Model>users=new ArrayList<Model>();
		Connection connection = ConnectDB.getConnect();
		String sql="SELECT * FROM user";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			ResultSet rs=preparedStatement.executeQuery();
		while(rs.next()) {
			Model user=new Model();
			user.setID(rs.getInt("ID"));
			user.setUSER(rs.getString("USERNAME"));
			user.setPASS(rs.getString("PASS"));
			user.setNAME(rs.getString("NAMAE"));
			user.setPHONE(rs.getString("PHONE"));
			user.setADDRESS(rs.getString("ADDRESS"));
			users.add(user);
		}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
			return users;
		}
	public static int addUsers(Model user) {
		int result=-1;
		Connection connection = ConnectDB.getConnect();
		String sqlcheck="SELECT USERNAME FROM user WHERE USERNAME=? ";

		String sqlinsert="INSERT INTO user(USERNAME,PASS,NAMAE,PHONE,ADDRESS) VALUE(?,?,?,?,?)";
		try {
			PreparedStatement ps= connection.prepareStatement(sqlcheck);
			ps.setString(1, user.getUSER());
		/*	int rs= preparedStatement.executeUpdate();*/
			ResultSet rs=ps.executeQuery();
			if (rs.next ()){
				result=-1;
			}else{	PreparedStatement preparedStatement=connection.prepareStatement(sqlinsert);
				preparedStatement.setString(1, user.getUSER());
				preparedStatement.setString(2, user.getPASS());
				preparedStatement.setString(3, user.getNAME());
				preparedStatement.setString(4, user.getPHONE());
				preparedStatement.setString(5, user.getADDRESS());
				result= preparedStatement.executeUpdate();
				result=1;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}return result;
	}
	public void update(Model user) {
		Connection connection = ConnectDB.getConnect();
		String sql="UPDATE user SET USERNAME=?,PASS=?,NAMAE=?,PHONE=?,ADDRESS=? WHERE ID=?";
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(sql);
			preparedStatement.setString(1, user.getUSER());
			preparedStatement.setString(2,user.getPASS());
			preparedStatement.setString(3, user.getNAME());
			preparedStatement.setString(4, user.getPHONE());
			preparedStatement.setString(5, user.getADDRESS());
			preparedStatement.setInt(6, user.getID());
			int rs= preparedStatement.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
        }
	public void delete(int id) {
		Connection connection = ConnectDB.getConnect();
		String sql="delete from user where ID=?";
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			int rs= preparedStatement.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}

	}
        public Model getid(int id){
		
		Connection connection = ConnectDB.getConnect();
		String sql="SELECT * FROM user where ID=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1,id);
			ResultSet rs=preparedStatement.executeQuery();
	
		while(rs.next()) {
			Model user=new Model();
                        user.setID(rs.getInt("ID"));
			user.setUSER(rs.getString("USERNAME"));
			user.setPASS(rs.getString("PASS"));
			user.setNAME(rs.getString("NAMAE"));
			user.setPHONE(rs.getString("PHONE"));
			user.setADDRESS(rs.getString("ADDRESS"));
                        return user;
		}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
			return null;
		}
           public Model getidbyusername(String username){
		
		Connection connection = ConnectDB.getConnect();
		String sql="SELECT * FROM user where USERNAME=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
                        preparedStatement.setString(1,username);
			ResultSet rs=preparedStatement.executeQuery();
	
		while(rs.next()) {
			Model user=new Model();
                        user.setID(rs.getInt("ID"));
			user.setUSER(rs.getString("USERNAME"));
			user.setPASS(rs.getString("PASS"));
			user.setNAME(rs.getString("NAMAE"));
			user.setPHONE(rs.getString("PHONE"));
			user.setADDRESS(rs.getString("ADDRESS"));
                        return user;
		}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
			return null;
		}
	}
		
	

